# Prueba reclutamiento

## Requerimeinto

Desarrolle una aplicación que exponga una API RESTful de creación de usuarios.
Todos los endpoints deben aceptar y retornar solamente JSON, inclusive al para los mensajes de error.
Todos los mensajes deben seguir el formato:
{"mensaje": "mensaje de error"}

## Registro
  Dicho endpoint deberá recibir un usuario con los campos "nombre", "correo", "contraseña",
  más un listado de objetos "teléfono", respetando el siguiente formato:

      {
      "name": "Juan Rodriguez",
      "email": "juan@rodriguez.org",
      "password": "hunter2",
      "phones": [
      {
      "number": "1234567",
      "citycode": "1",
      "contrycode": "57"
      }
      ]
      }

  ● Responder el código de status HTTP adecuado

  ● En caso de éxito, retornar el usuario y los siguientes campos:

      id: id del usuario (puede ser lo que se genera por el banco de datos, pero sería más deseable un UUID)
      created: fecha de creación del usuario
      modified: fecha de la última actualización de usuario
      last_login: del último ingreso (en caso de nuevo usuario, va a coincidir con la fecha de creación)
      token: token de acceso de la API (puede ser UUID o JWT)
      isactive: Indica si el usuario sigue habilitado dentro del sistema.
  
  ● En caso que el correo exista en la base de datos, deberá retornar un error "El correo ya registrado".
  
  ● El correo debe seguir una expresión regular para validar que formato sea el correcto. (aaaaaaa@dominio.cl)
  
  ● La clave debe seguir una expresión regular para validar que formato sea el correcto. (Una Mayúscula, letras minúsculas, y dos números)

  ● Se debe hacer traza de logs dentro del aplicativo.

  ● El token deberá ser persistido junto con el usuario

## Requisitos mandatorios
  ● Plazo: 2 días.

  ● Banco de datos en memoria.

  ● Gradle como herramienta de construcción.
  
  ● Pruebas unitarias (Deseable: Spock Framework).

  ● Persistencia con Hibernate.
  
  ● Framework Spring Boot.

  ● Java 8. (Usar más de dos características propias de la versión)
  
  ● Entrega en un repositorio público (github, gitlab o bitbucket) con el código fuente.

  ● Entregar diagrama de componentes de la solución y al menos un diagrama de secuencia (ambos diagramas son de carácter obligatorio y deben seguir estándares UML).

  ● README.md debe contener las instrucciones para levantar y usar el proyecto.

##Requisitos deseables

● JWT cómo token

#Pre-requisitos

- Java 8
- Gradle 6.6.1.

#Contenido proyecto

- SpringBoot 2.3.4
- h2 memory database.
- Spock Framework
- Open Api 3 - Swagger.
- Token JWT 

## Ejecutar Proyecto

Para ejecutar el proyecto, ejecutar el comando

$ gradle bootRun


## Detalles del Proyecto
El proyecto  expone dos servcios:


[POST] localhost:8080/authenticate/token

[POST] localhost:8080/user/user

Los cuales pueden ser probados mediante swagger en la siguiente URL

http://localhost:8080/swagger-ui/

El primer servicio es pre-requisito del segundo ya que se debe obtener el token y eso se logra enviando los siguientes datos:

  {
  "username":"pruebareclutamiento",
  "password":"password"
  }

Una vez que se tenga el token se debe ejecutar el segundo servicio agregando en el header del llamado el key Authorization, con el valor "Bearer " ejemplo:

Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwcnVlYmFyZWNsdXRhbWllbnRvIiwiZXhwIjoxNjAxODcyMzk3LCJpYXQiOjE2MDE4NTQzOTd9.34CKTNe-SIxf-k9ViLnMCwi22XsMivjSaGKxEzS2znpScvyFcuYFAZ0Cg49TJ6HlhOxlT3RAy44KyhDGpoD5Hw


Se debe completar el llamado con el siguiente cuerpo:

{
"name" : "Juan Rodriguez" ,
"email" : " juan@rodriguez.org " ,
"password" : "Hunter22" ,
"phones" : [{
"number" : "1234567" ,
"citycode" : "1" ,
"contrycode" : "57"
}]
}

Para efectos de prueba esta deshabiltado el pre-requisito del token, para habilitarlo se debe modificar el perfil activo en el archivo aplication.properties
pasando de: 

spring.profiles.active=dev

A:


spring.profiles.active=test

Para evidenciar la persistencia de datos se debe entrar a la consola de H2 mediante la siguiente url y datos de conexion:


http://localhost:8080/h2
Driver Class= rg.h2.Driver
JDBC URL= jdbc:h2:mem:registration_02;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
User Name=sa
Password=
