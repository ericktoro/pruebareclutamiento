import cl.globallogic.pruebarecluatamiento.mappers.PhoneDtoToEntityConvert
import cl.globallogic.pruebarecluatamiento.persistence.dao.UserRepository
import cl.globallogic.pruebarecluatamiento.rest.dto.PhoneDto
import cl.globallogic.pruebarecluatamiento.rest.dto.UserDto
import cl.globallogic.pruebarecluatamiento.rest.dto.RespuestaDto
import cl.globallogic.pruebarecluatamiento.persistence.model.User
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import cl.globallogic.pruebarecluatamiento.rest.controller.RegistrationRestController
import cl.globallogic.pruebarecluatamiento.service.UserService
import javax.servlet.http.HttpServletRequest

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class RegistrationRestControllerTest extends Specification {
    def request = new MockHttpServletRequest()
    def mockedUserRepository = Mock(UserRepository)
    def mockedPhoneDtoToEntityConvert= Mock(PhoneDtoToEntityConvert)
    def mockeUuserService = new UserService(mockedUserRepository,mockedPhoneDtoToEntityConvert)
    def RegistrationRestController registrationRestController = new RegistrationRestController(mockeUuserService)

def setup() {
   /*
   this.registrationRestController = new RegistrationRestController(userService)
    List<PhoneDto> phones = new ArrayList<>();
    phones.add(PhoneDto.builder().contrycode("+56").citycode("22").phoneNumber("123456789").build());
    userService.registerNewUserAccount(_)>> User.builder().id(UUID.randomUUID()).name("erick").password("Erick22").email("erick@erick.cl").phones(phones).build();
    */

}


def "resgitrar usuario"()

{
    given:
    List<PhoneDto> phones = new ArrayList<>();
    phones.add(PhoneDto.builder().contrycode("+56").citycode("22").phoneNumber("123456789").build());
    UserDto user = UserDto.builder().name("Erick").password("Erick").email("erick@erick.cl").phones(phones).build();
    when:
     RespuestaDto respuestaDto= registrationRestController.registerUserAccount(user, request);
    then:
    1 * mockedUserRepository.findByEmail('erick@erick.cl')
    1 * mockedUserRepository.save(_)
    1 * mockedPhoneDtoToEntityConvert.convert(_)
}


}
