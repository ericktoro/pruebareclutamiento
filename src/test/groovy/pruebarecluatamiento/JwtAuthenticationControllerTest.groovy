package pruebarecluatamiento


import cl.globallogic.pruebarecluatamiento.persistence.model.User
import cl.globallogic.pruebarecluatamiento.rest.controller.JwtAuthenticationController
import cl.globallogic.pruebarecluatamiento.rest.controller.RegistrationRestController
import cl.globallogic.pruebarecluatamiento.rest.dto.JwtRequest
import cl.globallogic.pruebarecluatamiento.rest.dto.PhoneDto
import cl.globallogic.pruebarecluatamiento.rest.dto.RespuestaDto
import cl.globallogic.pruebarecluatamiento.rest.dto.UserDto
import cl.globallogic.pruebarecluatamiento.service.UserService
import org.springframework.http.ResponseEntity
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.web.bind.annotation.RequestBody
import spock.lang.Specification

class JwtAuthenticationControllerTest extends Specification {

    def JwtAuthenticationController jwtAuthenticationController
    def UserDetailsService jwtInMemoryUserDetailsService = Mock(UserDetailsService)
    def AuthenticationManager authenticationManager= Stub(AuthenticationManager)
    def request = new MockHttpServletRequest()



def setup() {
    this.jwtAuthenticationController = new JwtAuthenticationController()

}

def "generar token"()

{
    given:
    JwtRequest authenticationRequest=new JwtRequest("pruebareclutamiento","password")
    when:
    ResponseEntity respose = this.jwtAuthenticationController.createAuthenticationToken(authenticationRequest)
    then:
    respuestaDto.getId()
}


}
