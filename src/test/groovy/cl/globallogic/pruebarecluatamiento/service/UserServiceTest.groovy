package cl.globallogic.pruebarecluatamiento.service

import cl.globallogic.pruebarecluatamiento.mappers.PhoneDtoToEntityConvert
import cl.globallogic.pruebarecluatamiento.persistence.dao.UserRepository
import cl.globallogic.pruebarecluatamiento.persistence.model.Phone
import cl.globallogic.pruebarecluatamiento.persistence.model.User
import cl.globallogic.pruebarecluatamiento.rest.dto.PhoneDto
import cl.globallogic.pruebarecluatamiento.rest.dto.UserDto
import cl.globallogic.pruebarecluatamiento.rest.error.BadCredentialsException
import cl.globallogic.pruebarecluatamiento.rest.error.UserAlreadyExistException
import spock.lang.Specification

import java.sql.Timestamp
import java.time.Instant

class UserServiceTest extends Specification {


    def "test registerNewUserAccount"() {
        given: "un usuario valido"
        def mockedUserRepository = Mock(UserRepository)
        def mockedPhoneDtoToEntityConvert= Mock(PhoneDtoToEntityConvert)
        def userService = new UserService(mockedUserRepository,mockedPhoneDtoToEntityConvert)

        List<PhoneDto> phonesDto = new ArrayList<>();
        phonesDto.add(PhoneDto.builder().contrycode("+56").citycode("22").phoneNumber("123456789").build());
        UserDto userDto = UserDto.builder().name("").password("Erick22").email("erick@erick.cl").phones(phonesDto).build();

        when:
        userService.registerNewUserAccount(userDto)
        then:
        1 * mockedUserRepository.findByEmail(_)
        1 * mockedPhoneDtoToEntityConvert.convert(_)
        1 * mockedUserRepository.save(_)
    }
    def "test registerNewUserAccount mail exitente"() {
        given: "un mail existente en la base de datos"
        List<Phone> phones = new ArrayList<>();
        phones.add(Phone.builder().contrycode("+56").citycode("22").number("123456789").build())
        User user = User.builder().active(true).email("erick@erick.cl").id(UUID.randomUUID()).lastLogin(Timestamp.from(Instant.now()))
                .name("erick").password("123")
                .phones(phones)
                .token("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwcnVlYmFyZWNsdXRhbWllbnRvIiwiZXhwIjoxNjAxODcyMzk3LCJpYXQiOjE2MDE4NTQzOTd9.34CKTNe-SIxf-k9ViLnMCwi22XsMivjSaGKxEzS2znpScvyFcuYFAZ0Cg49TJ6HlhOxlT3RAy44KyhDGpoD5Hw")
                .build()
        def mockedUserRepository = Stub(UserRepository){
            findByEmail(_)>> user
        }
        def mockedPhoneDtoToEntityConvert= Mock(PhoneDtoToEntityConvert)
        def userService = new UserService(mockedUserRepository,mockedPhoneDtoToEntityConvert)

        List<PhoneDto> phonesDto = new ArrayList<>();
        phonesDto.add(PhoneDto.builder().contrycode("+56").citycode("22").phoneNumber("123456789").build());
        UserDto userDto = UserDto.builder().name("").password("Erick22").email("erick@erick.cl").phones(phonesDto).build();

        when:
        userService.registerNewUserAccount(userDto)
        then:
         thrown(UserAlreadyExistException)
    }

}