package cl.globallogic.pruebarecluatamiento.service

import org.springframework.security.core.userdetails.User


import spock.lang.Specification

class JwtUserDetailsServiceTest extends Specification {
    def "test loadUserByUsername"() {
        given: "usuario valido"
        def usuario="pruebareclutamiento"
        JwtUserDetailsService jwtUserDetailsService = new JwtUserDetailsService()
        when:
        def respuesta=jwtUserDetailsService.loadUserByUsername(usuario)
        then:
        respuesta.getUsername()=="pruebareclutamiento"
    }
}
