package cl.globallogic.pruebarecluatamiento;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cl.globallogic.pruebarecluatamiento.persistence.model.Phone;
import cl.globallogic.pruebarecluatamiento.rest.dto.PhoneDto;
import cl.globallogic.pruebarecluatamiento.rest.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Slf4j
class RegistrationRestControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void registerUserAccount() throws Exception {
    String uri = "/users/user";
    List<PhoneDto> phones = new ArrayList<>();
    PhoneDto phone = new PhoneDto();
    phone.setContrycode("+56");
    phone.setCitycode("22");
    phone.setPhoneNumber("123456789");
    phones.add(phone);
    UserDto user = new UserDto();
    user.setName("Erick");
    user.setPassword("Erick22");
    user.setEmail("erick@erick.cl");
    user.setPhones(phones);
    ObjectMapper om = new ObjectMapper();
    om.setSerializationInclusion(Include.NON_NULL);
    String content = om.writeValueAsString(user);

    String response = mockMvc.perform(post(uri)
        .header("Authorization",
            "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwcnVlYmFyZWNsdXRhbWllbnRvIiwiZXhwIjoxNjAxODcyMzk3LCJpYXQiOjE2MDE4NTQzOTd9.34CKTNe-SIxf-k9ViLnMCwi22XsMivjSaGKxEzS2znpScvyFcuYFAZ0Cg49TJ6HlhOxlT3RAy44KyhDGpoD5Hw")
        .content(content)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(HttpStatus.CREATED.value()))
        .andReturn().getResponse().getContentAsString();

    log.info(response);

  }
  @Test
  public void registerUserAccountFallaNombre() throws Exception {
    String uri = "/users/user";
    List<PhoneDto> phones = new ArrayList<>();
    phones.add(PhoneDto.builder().contrycode("+56").citycode("22").phoneNumber("123456789").build());
    UserDto user = UserDto.builder().name("").password("Erick22").email("erick@erick.cl").phones(phones).build();
    ObjectMapper om = new ObjectMapper();
    om.setSerializationInclusion(Include.NON_NULL);
    String content = om.writeValueAsString(user);

    String response = mockMvc.perform(post(uri)
        .header("Authorization",
            "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwcnVlYmFyZWNsdXRhbWllbnRvIiwiZXhwIjoxNjAxODcyMzk3LCJpYXQiOjE2MDE4NTQzOTd9.34CKTNe-SIxf-k9ViLnMCwi22XsMivjSaGKxEzS2znpScvyFcuYFAZ0Cg49TJ6HlhOxlT3RAy44KyhDGpoD5Hw")
        .content(content)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(HttpStatus.CREATED.value()))
        .andReturn().getResponse().getContentAsString();

    log.info(response);

  }



}
