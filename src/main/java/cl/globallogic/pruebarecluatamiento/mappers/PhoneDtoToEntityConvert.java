package cl.globallogic.pruebarecluatamiento.mappers;


import cl.globallogic.pruebarecluatamiento.persistence.model.Phone;
import cl.globallogic.pruebarecluatamiento.rest.dto.PhoneDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * The type Phone dto to entity convert.
 */
@Component
public class PhoneDtoToEntityConvert implements Converter<PhoneDto, Phone> {

  @Override
  public Phone convert(PhoneDto source) {
    return Phone.builder().number(source.getPhoneNumber())
        .citycode(source.getCitycode()).contrycode(source.getContrycode()).build();
  }
}
