package cl.globallogic.pruebarecluatamiento.spring;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * The type H 2 config.
 */
@Configuration
@Order(1)
@Profile("dev")
public class H2Config extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    http.headers().frameOptions().disable();
    // @formatter:off
    http
        .csrf().disable()
        .authorizeRequests()
        .antMatchers("/h2/**").permitAll(); // to enable access to H2 db's console
    // @formatter:on
  }
}
