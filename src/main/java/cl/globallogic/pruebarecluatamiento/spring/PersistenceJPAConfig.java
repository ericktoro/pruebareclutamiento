package cl.globallogic.pruebarecluatamiento.spring;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * The type Persistence jpa config.
 */
@Configuration
@EnableTransactionManagement
public class PersistenceJPAConfig {

}