package cl.globallogic.pruebarecluatamiento.rest.dto;

import java.io.Serializable;


/**
 * The type Jwt response.
 *
 * @author Erick Toro Se ocupa para para mapear el response al solitar el token, aca viaja el token
 * de JWT
 */
//TODO: renombrar para seguir una nomenclatura estandar
public class JwtResponse implements Serializable {

  private static final long serialVersionUID = -8091879091924046844L;
  private final String jwttoken;

  /**
   * Instantiates a new Jwt response.
   *
   * @param jwttoken the jwttoken
   */
  public JwtResponse(String jwttoken) {
    this.jwttoken = jwttoken;
  }

  /**
   * Gets token.
   *
   * @return the token
   */
  public String getToken() {
    return this.jwttoken;
  }
}