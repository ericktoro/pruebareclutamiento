package cl.globallogic.pruebarecluatamiento.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The type Phone dto.
 *
 * @author Erick Toro Entidad utilizada para guardar los telefonos usuario estan asociadas mediante
 * el id de la entidad user
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Telefonos del usuario")
public class PhoneDto {

  @Schema(description = "Codigo de Pais")
  private String contrycode;
  @Schema(description = "Codigo de ciudad")
  private String citycode;
  @Schema(description = "Numero Telefonico")
  private String phoneNumber;


}
