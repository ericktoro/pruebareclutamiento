package cl.globallogic.pruebarecluatamiento.rest.dto;


import cl.globallogic.pruebarecluatamiento.validation.ValidEmail;
import cl.globallogic.pruebarecluatamiento.validation.ValidPassword;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The type User dto.
 *
 * @author Erick Toro Se ocupa para para mapear los datos de la llamada del servcio y  ademas de
 * validar que los datos esten correctos
 */
//TODO: renombrar para seguir una nomenclatura estanda
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(
    description = "Usuario"
)
public class UserDto {

  @NotNull
  @Size(min = 1, message = "Campo")
  @Schema(description = "Nombre del usuario", example = " ", required = true)
  private String name;
  @ValidPassword
  @Schema(description = "Password del usuario", example = " ", required = true)
  private String password;

  @ValidEmail
  @NotNull
  @Size(min = 1, message = "{Size.userDto.email}")
  @Schema(description = "Mails del usuario", example = " ", required = true)
  private String email;
  @Schema(description = "Telefonos del usuario", example = " ", required = false,oneOf = PhoneDto.class)
  private List<PhoneDto> phones;




}
