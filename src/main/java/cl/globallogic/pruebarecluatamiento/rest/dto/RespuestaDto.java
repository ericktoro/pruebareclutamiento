package cl.globallogic.pruebarecluatamiento.rest.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//TODO: renombrar para seguir una nomenclatura estandar

/**
 * The type Respuesta dto.
 *
 * @author Erick Toro Se ocupa para para mapear la respuesta del servcio al hacer una insercion
 * exitosa
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RespuestaDto {

  private String id;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
  private Date created;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
  private Date modified;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy'T'HH:mm:ss.SSSZ")
  private Date lastLogin;
  private String token;
  private Boolean active;

}
