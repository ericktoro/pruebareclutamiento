package cl.globallogic.pruebarecluatamiento.rest.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


/**
 * The type Web security config test.
 *
 * @author Erick Toro Configuracion de seguridad  esta  solo esta activa si el profile de test esta
 * activo, esto para no casuar problemas en las pruebas
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Profile("test")
public class WebSecurityConfigTest extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity httpSecurity) throws Exception {
    httpSecurity.headers().frameOptions().disable();
    // We don't need CSRF for this example
    httpSecurity.csrf().disable().authorizeRequests().anyRequest().permitAll();
  }
}
