package cl.globallogic.pruebarecluatamiento.rest.util;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.validation.ObjectError;

/**
 * The type Generic response.
 */
public class GenericResponse {

  private String message;
  private String error;

  /**
   * Instantiates a new Generic response.
   *
   * @param message the message
   */
  public GenericResponse(final String message) {
    super();
    this.message = message;
  }

  /**
   * Instantiates a new Generic response.
   *
   * @param message the message
   * @param error   the error
   */
  public GenericResponse(final String message, final String error) {
    super();
    this.message = message;
    this.error = error;
  }

  /**
   * Instantiates a new Generic response.
   *
   * @param allErrors the all errors
   * @param error     the error
   */
  public GenericResponse(List<ObjectError> allErrors, String error) {
    this.error = error;
    String temp = allErrors.stream().map(e -> {
      return e.getDefaultMessage();
    }).collect(Collectors.joining(","));
    this.message = temp;
  }

  /**
   * Gets message.
   *
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * Sets message.
   *
   * @param message the message
   */
  public void setMessage(final String message) {
    this.message = message;
  }

  /**
   * Gets error.
   *
   * @return the error
   */
  public String getError() {
    return error;
  }

  /**
   * Sets error.
   *
   * @param error the error
   */
  public void setError(final String error) {
    this.error = error;
  }

}
