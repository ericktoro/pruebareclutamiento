package cl.globallogic.pruebarecluatamiento.rest.controller;

import cl.globallogic.pruebarecluatamiento.persistence.model.User;
import cl.globallogic.pruebarecluatamiento.rest.dto.RespuestaDto;
import cl.globallogic.pruebarecluatamiento.rest.dto.UserDto;
import cl.globallogic.pruebarecluatamiento.service.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * The type Registration rest controller.
 *
 * @author Erick Toro Controlador rest que que resgitra un usuario para cumplir con lo solicitado en
 * la prueba de reclutamiento se apoya en JPA y Hibernte  para persistir el usuario, valida que mail
 * del usaurio no haya sido agregado anteriormente ademas de las validaciones de campos solicitadas
 * en el enunciado de la prueba, debe ejecutarse con el token de JWT
 */
@Slf4j
@RestController
@RequestMapping(value = "/users")
public class RegistrationRestController {


  private IUserService userService;

  /**
   * Instantiates a new Registration rest controller.
   */
  public RegistrationRestController(IUserService userService) {
    super();
    this.userService=userService;
  }


  /**
   * Register user account respuesta dto.
   *
   * @param accountDto the account dto
   * @param request    the request
   * @return the respuesta dto
   */
  @PostMapping(value = "/user")
  @Operation(
      description = "Servicio encargado de resgitrar un usuario",
      summary= "Permite registrar un usuario siempre y cuando el mail no este ya registrado."
  )
  public RespuestaDto registerUserAccount(@Validated @RequestBody final UserDto accountDto,
      final HttpServletRequest request) {
    RespuestaDto respuesta = new RespuestaDto();
    log.debug("Registering user account with information: {}", accountDto);
    User registered = userService.registerNewUserAccount(accountDto);
    if (registered != null) {
      respuesta.setId(registered.getId().toString());
      respuesta.setCreated(registered.getCreated());
      respuesta.setModified(registered.getModified());
      respuesta.setLastLogin(registered.getCreated());
      //Para efectos de prueba se deja como opcional el seteo del token
      Optional<String> requestTokenHeader = Optional.ofNullable(request.getHeader("Authorization"));
      respuesta.setToken(requestTokenHeader.isPresent()? requestTokenHeader.get().substring(7):"");
      respuesta.setActive(registered.getActive());
      return respuesta;
    }
    return respuesta;
  }


}
