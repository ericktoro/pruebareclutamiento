package cl.globallogic.pruebarecluatamiento.rest.controller;

import cl.globallogic.pruebarecluatamiento.rest.dto.JwtRequest;
import cl.globallogic.pruebarecluatamiento.rest.dto.JwtResponse;
import cl.globallogic.pruebarecluatamiento.rest.security.JwtTokenUtil;
import io.swagger.v3.oas.annotations.Operation;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * The type Jwt authentication controller.
 *
 * @author Erick Toro Controlador rest que se encarga de autenticar y y generar el token de Jwt este
 * se desahilita cuando se activa el profile test para no entorpecer las pruebas
 */
@Slf4j
@RestController
@CrossOrigin
@Profile("!test")
@RequestMapping(value = "/authenticate")
public class JwtAuthenticationController {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Autowired
  private UserDetailsService jwtInMemoryUserDetailsService;

  /**
   * Create authentication token response entity.
   *
   * @param authenticationRequest the authentication request
   * @return the response entity
   * @throws Exception the exception
   */
  @PostMapping(value = "/token")
  @Operation(
      description = "Servicio encargado de autenticar y generar token",
      summary= "Permite validar un usuario y genera el token correpondiente."
  )
  public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest)
      throws BadCredentialsException,DisabledException {

    authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

    final UserDetails userDetails = jwtInMemoryUserDetailsService
        .loadUserByUsername(authenticationRequest.getUsername());

    final String token = jwtTokenUtil.generateToken(userDetails);

    return ResponseEntity.ok(new JwtResponse(token));
  }


  private void authenticate(String username, String password) throws BadCredentialsException,DisabledException {

    try {

      Objects.requireNonNull(username);
      Objects.requireNonNull(password);
    } catch (Exception e) {
      throw new BadCredentialsException("Invalid User/Password");
    }

    try {
      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    } catch (DisabledException e) {
      throw new BadCredentialsException("User Disabled: " + username);
    } catch (BadCredentialsException e) {
      throw new BadCredentialsException("Invalid Credential: " + username);
    }
  }
}
