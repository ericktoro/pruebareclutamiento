package cl.globallogic.pruebarecluatamiento.rest.error;


/**
 * The type User not found exception.
 *
 * @author Erick Toro Se ocupa para para manejar un tipo customizado de excepcion
 */
public final class UserNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 5861310537366287163L;

  /**
   * Instantiates a new User not found exception.
   */
  public UserNotFoundException() {
    super();
  }

  /**
   * Instantiates a new User not found exception.
   *
   * @param message the message
   * @param cause   the cause
   */
  public UserNotFoundException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * Instantiates a new User not found exception.
   *
   * @param message the message
   */
  public UserNotFoundException(final String message) {
    super(message);
  }

  /**
   * Instantiates a new User not found exception.
   *
   * @param cause the cause
   */
  public UserNotFoundException(final Throwable cause) {
    super(cause);
  }

}
