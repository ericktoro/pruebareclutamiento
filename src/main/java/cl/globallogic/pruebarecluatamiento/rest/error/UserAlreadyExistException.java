package cl.globallogic.pruebarecluatamiento.rest.error;


/**
 * The type User already exist exception.
 *
 * @author Erick Toro Se ocupa para para manejar un tipo customizado de excepcion
 */
public final class UserAlreadyExistException extends RuntimeException {

  private static final long serialVersionUID = 5861310537366287163L;

  /**
   * Instantiates a new User already exist exception.
   */
  public UserAlreadyExistException() {
    super();
  }

  /**
   * Instantiates a new User already exist exception.
   *
   * @param message the message
   * @param cause   the cause
   */
  public UserAlreadyExistException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * Instantiates a new User already exist exception.
   *
   * @param message the message
   */
  public UserAlreadyExistException(final String message) {
    super(message);
  }

  /**
   * Instantiates a new User already exist exception.
   *
   * @param cause the cause
   */
  public UserAlreadyExistException(final Throwable cause) {
    super(cause);
  }

}
