package cl.globallogic.pruebarecluatamiento.rest.error;


/**
 * The type Bad credentials exception.
 *
 * @author Erick Toro Se ocupa para para manejar un tipo customizado de excepcion
 */
public final class BadCredentialsException extends RuntimeException {

  private static final long serialVersionUID = 5861310537366287163L;

  /**
   * Instantiates a new Bad credentials exception.
   */
  public BadCredentialsException() {
    super();
  }

  /**
   * Instantiates a new Bad credentials exception.
   *
   * @param message the message
   * @param cause   the cause
   */
  public BadCredentialsException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * Instantiates a new Bad credentials exception.
   *
   * @param message the message
   */
  public BadCredentialsException(final String message) {
    super(message);
  }

  /**
   * Instantiates a new Bad credentials exception.
   *
   * @param cause the cause
   */
  public BadCredentialsException(final Throwable cause) {
    super(cause);
  }

}
