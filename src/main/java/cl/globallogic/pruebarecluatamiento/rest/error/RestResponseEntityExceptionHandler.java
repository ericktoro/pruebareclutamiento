package cl.globallogic.pruebarecluatamiento.rest.error;

import cl.globallogic.pruebarecluatamiento.rest.util.GenericResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


/**
 * The type Rest response entity exception handler.
 *
 * @author Erick Toro  Encargado de manejar los tipos de excepciones dentro de la aplicacion y
 * formato de mensaje
 */
@Slf4j
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

  @Autowired
  private MessageSource messages;

  /**
   * Instantiates a new Rest response entity exception handler.
   */
  public RestResponseEntityExceptionHandler() {
    super();
  }

  // API

  // 400
  @Override
  protected ResponseEntity<Object> handleBindException(final BindException ex,
      final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
    log.error("400 Status Code", ex);
    final BindingResult result = ex.getBindingResult();
    final GenericResponse bodyOfResponse = new GenericResponse(result.getAllErrors(),
        "Invalid" + result.getObjectName());
    return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST,
        request);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status,
      final WebRequest request) {
    log.error("400 Status Code", ex);
    final BindingResult result = ex.getBindingResult();
    final GenericResponse bodyOfResponse = new GenericResponse(result.getAllErrors(),
        "Invalid" + result.getObjectName());
    return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST,
        request);
  }


  /**
   * Handle user not found response entity.
   *
   * @param ex      the ex
   * @param request the request
   * @return the response entity
   */
// 404
  @ExceptionHandler({UserNotFoundException.class})
  public ResponseEntity<Object> handleUserNotFound(final RuntimeException ex,
      final WebRequest request) {
    log.error("404 Status Code", ex);
    final GenericResponse bodyOfResponse = new GenericResponse(
        messages.getMessage("message.userNotFound", null, request.getLocale()), "UserNotFound");
    return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND,
        request);
  }

  /**
   * Handle user already exist response entity.
   *
   * @param ex      the ex
   * @param request the request
   * @return the response entity
   */
// 409
  @ExceptionHandler({UserAlreadyExistException.class})
  public ResponseEntity<Object> handleUserAlreadyExist(final RuntimeException ex,
      final WebRequest request) {
    log.error("409 Status Code", ex);
    final GenericResponse bodyOfResponse = new GenericResponse(
        messages.getMessage("message.userAlreadyExist", null, request.getLocale()), "UserAlreadyExist");
    return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT,
        request);
  }

  /**
   * Handle bad credentials response entity.
   *
   * @param ex      the ex
   * @param request the request
   * @return the response entity
   */
// 409
  @ExceptionHandler({BadCredentialsException.class})
  public ResponseEntity<Object> handleBadCredentials(final RuntimeException ex,
      final WebRequest request) {
    log.error("503 Status Code", ex);
    final GenericResponse bodyOfResponse = new GenericResponse(
        messages.getMessage("message.unathorized", null, request.getLocale()), "BadCredentials");
    return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.UNAUTHORIZED,
        request);
  }


  /**
   * Handle internal response entity.
   *
   * @param ex      the ex
   * @param request the request
   * @return the response entity
   */
  @ExceptionHandler({Exception.class})
  public ResponseEntity<Object> handleInternal(final RuntimeException ex,
      final WebRequest request) {
    log.error("500 Status Code", ex);
    final GenericResponse bodyOfResponse = new GenericResponse(
        messages.getMessage("message.error", null, request.getLocale()), "InternalError");
    return new ResponseEntity<>(bodyOfResponse, new HttpHeaders(),
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

}
