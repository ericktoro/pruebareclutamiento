package cl.globallogic.pruebarecluatamiento.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The type Phone.
 *
 * @author Erick Toro Entidad utilizada para guardar los telefonos usuario estan asociadas mediante
 * el id de la entidad user
 */
@Entity
@Table(name = "user_phone")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Phone {

  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String number;
  private String citycode;
  private String contrycode;

}
