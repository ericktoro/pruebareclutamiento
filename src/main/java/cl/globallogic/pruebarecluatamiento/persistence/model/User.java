package cl.globallogic.pruebarecluatamiento.persistence.model;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;


/**
 * The type User.
 *
 * @author Erick Toro Entidad que se usa para persisitir los datos del usuario
 */
@Entity
@Table(name = "user")
@Getter
@Setter
@SuperBuilder
public class User extends AuditModel {

  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  @Column(name = "user_id", updatable = false, nullable = false)
  private UUID id;

  private String name;


  private String email;

  @Column(length = 60)
  private String password;

  private Timestamp lastLogin;
  private String token;
  private Boolean active;


  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "user_id")
  private List<Phone> phones;


  /**
   * Instantiates a new User.
   */
  public User() {
    super();
    this.active = false;
  }


}