package cl.globallogic.pruebarecluatamiento.persistence.dao;

import cl.globallogic.pruebarecluatamiento.persistence.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface User repository.
 */
public interface UserRepository extends JpaRepository<User, Long> {

  /**
   * Find by email user.
   *
   * @param email the email
   * @return the user
   */
  User findByEmail(String email);

  @Override
  void delete(User user);

}
