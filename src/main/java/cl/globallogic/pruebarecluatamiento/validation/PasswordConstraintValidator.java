package cl.globallogic.pruebarecluatamiento.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * The type Password constraint validator.
 */
public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

	 private static final String PASSWORD_PATTERN = "^(?=(?:.*?[A-Z-]))(?=.*?[a-z-])(?=(?:.*?[0-9]){2}).*$";

	    private static final Pattern PATTERN = Pattern.compile(PASSWORD_PATTERN);

	    @Override
	    public boolean isValid(final String password, final ConstraintValidatorContext context) {
	        return (validatePassword(password));
	    }

	    private boolean validatePassword(final String password) {
	        Matcher matcher = PATTERN.matcher(password);
	        return matcher.matches();
	    }
	
	

}
