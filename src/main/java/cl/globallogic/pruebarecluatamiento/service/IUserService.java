package cl.globallogic.pruebarecluatamiento.service;

import cl.globallogic.pruebarecluatamiento.persistence.model.User;
import cl.globallogic.pruebarecluatamiento.rest.dto.UserDto;
import cl.globallogic.pruebarecluatamiento.rest.error.UserAlreadyExistException;
import java.util.Optional;

/**
 * The interface User service.
 */
public interface IUserService {

  /**
   * Register new user account user.
   *
   * @param accountDto the account dto
   * @return the user
   * @throws UserAlreadyExistException the user already exist exception
   */
  User registerNewUserAccount(UserDto accountDto) throws UserAlreadyExistException;

  /**
   * Save registered user.
   *
   * @param user the user
   */
  void saveRegisteredUser(User user);

  /**
   * Find user by email user.
   *
   * @param email the email
   * @return the user
   */
  User findUserByEmail(String email);

  /**
   * Gets user by id.
   *
   * @param id the id
   * @return the user by id
   */
  Optional<User> getUserByID(long id);

}
