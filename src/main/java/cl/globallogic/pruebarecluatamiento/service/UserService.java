package cl.globallogic.pruebarecluatamiento.service;

import cl.globallogic.pruebarecluatamiento.mappers.PhoneDtoToEntityConvert;
import cl.globallogic.pruebarecluatamiento.persistence.dao.UserRepository;
import cl.globallogic.pruebarecluatamiento.persistence.model.User;
import cl.globallogic.pruebarecluatamiento.rest.dto.UserDto;
import cl.globallogic.pruebarecluatamiento.rest.error.UserAlreadyExistException;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 * The type User service.
 */
@Service
@Transactional
public class UserService implements IUserService {

  private UserRepository userRepository;
  private PhoneDtoToEntityConvert phoneDtoToEntityConvert;

  public UserService(UserRepository userRepository,PhoneDtoToEntityConvert phoneDtoToEntityConvert) {
    this.userRepository = userRepository;
    this.phoneDtoToEntityConvert=phoneDtoToEntityConvert;
  }

  @Override
  public User registerNewUserAccount(final UserDto accountDto) {
    if (emailExists(accountDto.getEmail())) {
        throw new UserAlreadyExistException();
    }
    final User user = new User();

    user.setName(accountDto.getName());
    user.setPassword(accountDto.getPassword());
    user.setEmail(accountDto.getEmail());
    user.setPhones(accountDto.getPhones().stream().map(this.phoneDtoToEntityConvert::convert)
        .collect(Collectors.toList()));

    return userRepository.save(user);
  }

  @Override
  public void saveRegisteredUser(final User user) {
    userRepository.save(user);
  }


  @Override
  public User findUserByEmail(final String email) {
    return userRepository.findByEmail(email);
  }

  @Override
  public Optional<User> getUserByID(final long id) {
    return userRepository.findById(id);
  }


  private boolean emailExists(final String email) {
    return userRepository.findByEmail(email) != null;
  }


}
