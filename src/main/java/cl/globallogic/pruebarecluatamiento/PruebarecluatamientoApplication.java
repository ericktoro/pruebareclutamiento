package cl.globallogic.pruebarecluatamiento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;


/**
 * The type Pruebarecluatamiento application.
 */
@SpringBootApplication
@EnableJpaAuditing
public class PruebarecluatamientoApplication {

  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(PruebarecluatamientoApplication.class, args);
  }

}
